#!/bin/bash

# Install composer.
install_composer() {
    information "Composer is not installed. Attempting to automatically install."
    curl -sS https://getcomposer.org/installer | php

    if [ $? -eq 1 ] || [ ! -f "composer.phar" ]; then
        warning "Could not install Composer."
        exit
    fi

    COMPOSER_CMD="$(which php) composer.phar"

    success "Installed Composer."
}

# Try to determine the PHP version and checking if it's high enough.
check_php_version() {
    PHP_VERSION=$(php -v | grep -Eow '^PHP [^ ]+' | cut -d' ' -f 2)

    if [ $? -eq 1 ]; then
        warning "PHP is not installed."
        exit 1
    fi

    vercomp "$PHP_VERSION" "$MIN_PHP_VERSION"
    if [ $? -eq 2 ]; then
        warning "Your PHP version is $PHP_VERSION. Minimum PHP version is $MIN_PHP_VERSION."
        exit 1
    fi
}

# determine wether or not to install composer. If it is installed, update it.
install_or_update_composer() {
    type composer > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        install_composer
    else
        sudo "$COMPOSER_CMD" self-update >> "$LOGFILE" 2>&1

        if [ $? -eq 1 ]; then
            warning "Failed to update composer. Continuing anyway."
        else
            success "Updated composer succesfully."
        fi
    fi
}

# Check for requirements before doing anything.
check_requirements() {
    information "Checking requirements"
    check_php_version
    install_or_update_composer
}

# Clone bedrock.
clone_bedrock() {
    # Backing up our .gitignore file
    mv .gitignore .bakgitignore

    information "Cloning bedrock"

    # Add bedrock temporarily as a remote to easily clone into existing non-empty directory.
    git init >> "$LOGFILE" 2>&1 &&
    git remote add origin $BEDROCK_REPO  >> "$LOGFILE" 2>&1 &&
    git fetch --all >> "$LOGFILE" 2>&1 &&
    git checkout -t origin/master >> "$LOGFILE" 2>&1 &&
    rm -rf .git/ >> "$LOGFILE" 2>&1

    # Errors occured while cloning bedrock
    if [ $? -eq 1 ]; then
        warning "Could not clone bedrock. Aborting."
        rm -rf .git/
        exit 1
    fi

    # Appending our gitignore rules to bedrock's .gitignore file
    cat .bakgitignore >> .gitignore && rm -f .bakgitignore

    success "Cloned bedrock"
}

install_bedrock() {
    information "Installing bedrock"

    # Install composer
    $COMPOSER_CMD install

    success "Installed bedrock"
}

prepare_for_install() {
    # Replace bedrock's example dotEnv file with ours.
    rm -f ./.env.example
    cp createfiles/.env.example .
}

extra_script() {
    if [ -f $1 ]; then
        information "Running extra script $1."

        # Run extra script
        $BASH_CMD $1 > /dev/stdout 2> "$TMPERRFILE"

        if [ $? -eq 1 ]; then
            if [ -f "$TMPERRFILE" ]; then
                # Process exited with an error AND has an error message
                warning "$(cat $TMPERRFILE)"
            fi
            warning "There were errors with the extra script $1."
        else
            # Process exited normally
            success "Successfully ran extra script $1."
        fi
    fi

    # Purge possible error message file
    if [ -f "$TMPERRFILE" ]; then
        rm -f "$TMPERRFILE"
    fi
}

# Loop over extra scripts and ask to execute them.
extra_scripts() {
    for FILENAME in extra-scripts/*.sh; do
        # get script usage
        SCRIPT_USAGE=$($BASH_CMD $FILENAME usage)

        # if this scripted is only used as an install script, continue to the next one.
        if [ "$SCRIPT_USAGE" == "install" ]; then
            continue
        fi

        # get script help
        SCRIPT_HELP=$($BASH_CMD $FILENAME help)

        information "Execute $(basename $FILENAME)? $SCRIPT_HELP."

        # determine whether or not to execute script.
        continue_or_not

        if [ $? -eq 1 ]; then
            continue
        fi

        # execute script
        extra_script "$FILENAME"
    done
}

# Main script
create() {
    # empty create log
    echo -n "" > "$LOGFILE"

    # init
    information "Welcome to the installer. Attempting to gain superuser permissions. Please enter your password if prompted. "
    sudo -v

    # check for requirements
    check_requirements

    # clone into bedrock
    clone_bedrock

    # prepare
    prepare_for_install

    # install bedrock
    install_bedrock

    # run scripts
    extra_scripts
}

# Includes
source createfiles/includes.sh

# Run script
create