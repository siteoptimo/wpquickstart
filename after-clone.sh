#!/bin/bash

install_composer() {
    information "Composer is not installed. Attempting to automatically install."
    curl -sS https://getcomposer.org/installer | php

    if [ $? -eq 1 ] || [ ! -f "composer.phar" ]; then
        warning "Could not install Composer."
        exit
    fi

    COMPOSER_CMD="$(which php) composer.phar"

    success "Installed Composer."
}

check_php_version() {
    PHP_VERSION=$(php -v | grep -Eow '^PHP [^ ]+' | gawk '{ print $2 }')

    if [ $? -eq 1 ]; then
        warning "PHP is not installed."
        exit 1
    fi

    vercomp "$PHP_VERSION" "$MIN_PHP_VERSION"
    if [ $? -eq 2 ]; then
        warning "Your PHP version is $PHP_VERSION. Minimum PHP version is $MIN_PHP_VERSION."
        exit 1
    fi
}

install_or_update_composer() {
    type composer > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        install_composer
    else
        sudo "$COMPOSER_CMD" self-update >> "$LOGFILE" 2>&1

        if [ $? -eq 1 ]; then
            warning "Failed to update composer. Continuing anyway."
        else
            success "Updated composer succesfully."
        fi
    fi
}

check_requirements() {
    information "Checking requirements"
    check_php_version
    install_or_update_composer
}

clone_bedrock() {
    mv .gitignore .bakgitignore

    information "Cloning bedrock"
    git init >> "$LOGFILE" 2>&1 &&
    git remote add origin $BEDROCK_REPO  >> "$LOGFILE" 2>&1 &&
    git fetch --all >> "$LOGFILE" 2>&1 &&
    git checkout -t origin/master >> "$LOGFILE" 2>&1 &&
    rm -rf .git/ >> "$LOGFILE" 2>&1

    if [ $? -eq 1 ]; then
        warning "Could not clone bedrock. Aborting."
        rm -rf .git/
        exit 1
    fi

    cat .bakgitignore >> .gitignore && rm -f .bakgitignore

    success "Cloned bedrock"
}

install_bedrock() {
    information "Installing bedrock"

    $COMPOSER_CMD install

    success "Installed bedrock"
}

prepare_for_install() {
    rm -f ./.env.example
    cp createfiles/.env.example .
}

extra_script() {
    if [ -f $1 ]; then
        information "Running extra script $1."

        $BASH_CMD $1 > /dev/stdout 2> "$TMPERRFILE"

        if [ $? -eq 1 ]; then
            if [ -f "$TMPERRFILE" ]; then
                warning "$(cat $TMPERRFILE)"
            fi
            warning "There were errors with the extra script $1."
        else
            success "Successfully ran extra script $1."
        fi
    fi

    if [ -f "$TMPERRFILE" ]; then
        rm -f "$TMPERRFILE"
    fi
}

extra_scripts() {
    for FILENAME in extra-scripts/*.sh; do
        SCRIPT_HELP=$($BASH_CMD $FILENAME help)

        information "Execute $(basename $FILENAME)? $SCRIPT_HELP."

        continue_or_not

        if [ $? -eq 0 ]; then
            extra_script "$FILENAME"
        fi
    done
}

after_clone() {
    # init
    information "Welcome to the after-clone installer. Attempting to gain superuser permissions. Please enter your password if prompted. "
    sudo -v

    # check for requirements
    check_requirements

    # prepare
    prepare_everything

    # run composer install
    composer_install

    # prepare
    prepare_for_install

    # install bedrock
    install_bedrock

    # run scripts
    extra_scripts
}

# Includes
source createfiles/includes.sh

# Run script
after_clone