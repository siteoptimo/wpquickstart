#!/bin/bash

errcho() {
    >&2 echo $@
}

check_requirements() {
    if [ ! -f .env ]; then
        errcho "Please set up your environment and create a dotenv file first."
        exit 1
    fi
}

new_git_repo() {
    git init
    git add --all
    git commit -m "Initial commit."
}

git_add_remote() {
    information "Do you want to add a new git remote repository?"

    continue_or_not

    if [ "$?" -eq 1 ]; then
        information "Alrighty then, have it your way."

        return 1
    fi

    read -e -p "$PROMPT What's the name of the remote? " -i "origin" REMOTE_NAME 2>&1
    read -e -p "$PROMPT What's the remote repository location? Unsure? Head over to Bitbucket/GitHub and create that repository. " REMOTE_LOCATION 2>&1

    git remote add "$REMOTE_NAME" "$REMOTE_LOCATION" &&
    git push -u "$REMOTE_NAME" master

    if [ "$?" -eq 0 ]; then
        success "Succesfully added remote repository and pushed to it."
    else
        warning "Could not succesfully add git remote."
    fi
}

init_git() {
    # checking requirements
    check_requirements

    # includes
    source createfiles/includes.sh

    # init git
    new_git_repo

    # add remote
    git_add_remote
}

if [ "$1" = "help" ]; then
    echo "Adds a git remote for you"
elif [ "$1" = "usage" ]; then
    echo "create"
else
    init_git
fi