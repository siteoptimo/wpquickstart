#!/bin/bash

APACHE_VERSION=
FILE_LOCATION=

check_requirements() {
    if [ ! -f .env ]; then
        errcho "Please set up your environment and create a dotenv file first."
        exit 1
    fi

    APACHE_VERSION=$(httpd -v | grep -Eow 'Apache/[^ ]+' | cut -d'/' -f 2)
    if [ $? -eq 1 ]; then
        errcho "Apache is not installed."
        exit 1
    fi
}

restart() {
    source ./createfiles/includes.sh

    check_requirements

    source .env

    if [ -d "/Applications/XAMPP" ]; then
        # detected xampp
        cd "/Applications/XAMPP/xamppfiles"
        ./xampp restart
        cd "$CWD"
    else
        (type systemctl > /dev/null 2>&1 && sudo systemctl restart httpd > /dev/null 2>&1 ) ||
        (type service > /dev/null 2>&1  && sudo service httpd restart > /dev/null 2>&1 ) ||
        (type apachectl > /dev/null 2>&1  && sudo apachectl restart > /dev/null 2>&1 ) ||
        (type apache2ctl > /dev/null 2>&1  && sudo apache2ctl restart > /dev/null 2>&1 )
    fi
}

if [ "$1" = "help" ]; then
    echo "Restarts Apache for you"
elif [ "$1" = "usage" ]; then
    echo "both"
else
    restart
    exit 0
fi