#!/bin/bash

APACHE_VERSION=
FILE_LOCATION=

check_requirements() {
    if [ ! -f .env ]; then
        errcho "Please set up your environment and create a dotenv file first."
        exit 1
    fi
}

create() {
    source ./createfiles/includes.sh

    check_requirements

    source .env

    read -e -p "$PROMPT What is your mysql admin username? " -i "root" MYSQL_USER 2>&1

    MYSQL=$(which mysql)
    QUERY="CREATE DATABASE IF NOT EXISTS $DB_NAME;"

    if [ "$MYSQL_USER" != "$DB_USER" ]; then
        QUERY="${QUERY}GRANT ALL ON *.* TO '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASS';FLUSH PRIVILEGES;"
    fi

    information "Please provide your MySQL password for the user '$MYSQL_USER'."

    $MYSQL -u"$MYSQL_USER" -p -e "$QUERY"
}

if [ "$1" = "help" ]; then
    echo "Creates the database and user for you (only if you're not using the mysql admin user)"
elif [ "$1" = "usage" ]; then
    echo "both"
else
    create
    exit 0
fi