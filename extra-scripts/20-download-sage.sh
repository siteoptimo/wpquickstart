#!/bin/bash

SAGE_REPO=https://github.com/roots/sage.git

clone_sage() {
    git clone $SAGE_REPO $THEME_PATH > /dev/null 2>&1 &&
    rm -rf "$THEME_PATH/.git" > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        errcho "Failed to clone into sage repository."
        exit 1
    fi
}

errcho() {
    >&2 echo $@
}

check_requirements() {
    if [ ! -f .env ]; then
        errcho "Please set up your environment and create a dotenv file first."
        exit 1
    fi
}

extra_checks() {
    if [ -d "$THEME_PATH" ]; then
        errcho "Your theme folder already exists. Aborting."
        exit 1
    fi
}

prepare_sage() {
    sed -i -e "s@http://example.dev@$WP_HOME@g" "$THEME_PATH/assets/manifest.json"
}

download_sage() {
    # checking requirements
    check_requirements

    # sourcing env file
    source .env

    # performing extra checks before cloning sage
    extra_checks

    # cloning sage
    clone_sage

    # prepare sage
    prepare_sage
}

if [ "$1" = "help" ]; then
    echo "Downloads the sage theme for you"
elif [ "$1" = "usage" ]; then
    echo "create"
else
    download_sage
fi