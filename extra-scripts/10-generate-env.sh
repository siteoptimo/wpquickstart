#!/bin/bash

declare -A variables
declare -a var_keys

ENV_FILE=.env
SALTS_LOCATION="https://www.siteoptimo.com/salts.php"

add_var() {
    variables["$1"]="$2"
    var_keys+=( "$1" )
}

generate() {
    source includes.sh

    # Ask for project name
    read -e -p "$PROMPT What's the project name? " -i "example" PROJECT_NAME 2>&1

    # Default values
    add_var "DB_NAME" "$PROJECT_NAME"
    add_var "DB_USER" "$PROJECT_NAME"
    add_var "DB_PASSWORD" ""
    add_var "DB_HOST" "localhost"

    ## Local env
    add_var "LOCAL_HOST" "$PROJECT_NAME.dev"
    add_var "FILES_PATH" "web/app/uploads"

    ## WP Envs
    add_var "WP_ENV" "development"
    add_var "WP_HOME" "http://$PROJECT_NAME.dev"
    add_var "WP_SITEURL" "http://$PROJECT_NAME.dev/wp"
    add_var "WP_PATH" "web/wp"
    add_var "THEME_PATH" "web/app/themes/$PROJECT_NAME"

    # Create file
    touch "$ENV_FILE"

    # Ask for values
    for KEY in "${!var_keys[@]}"; do
        VAR_KEY="${var_keys[$KEY]}"
        DEF_VALUE="${variables[$VAR_KEY]}"

        read -e -p "$PROMPT Value for $VAR_KEY? " -i "$DEF_VALUE" VALUE 2>&1

        variables[$KEY]="$VALUE"
    done

    # Print variables
    print_variables
}

print_variables() {
    # Prints variables to $ENV_FILE
    echo -n "" > "$ENV_FILE"
    for KEY in "${!var_keys[@]}"; do
        VAR_KEY="${var_keys[$KEY]}"
        DEF_VALUE="${variables[$VAR_KEY]}"

        echo "$VAR_KEY=$DEF_VALUE" >> "$ENV_FILE"
    done

    information "Fetching salts..."

    # Fetches salts and adds them to the file as well
    SALTS=$(wget "$SALTS_LOCATION" -q -O -)

    echo "${SALTS//\' /\'\n}" >> "$ENV_FILE"
}

if [ "$1" = "help" ]; then
    echo "Generates the .env file for you"
elif [ "$1" = "usage" ]; then
    echo "both"
else
    generate
    exit 0
fi