#!/bin/bash

APACHE_VERSION=
FILE_LOCATION=

check_requirements() {
    if [ ! -f .env ]; then
        errcho "Please set up your environment and create a dotenv file first."
        exit 1
    fi

    APACHE_VERSION=$(httpd -v | grep -Eow 'Apache/[^ ]+' | cut -d'/' -f 2)
    if [ $? -eq 1 ]; then
        errcho "Apache is not installed."
        exit 1
    fi
}

get_file_location() {
    APACHE_CONF_DIR=$(locate httpd.conf | head -n 1 | xargs dirname)
    FILE_LOCATION=$(find "$APACHE_CONF_DIR" -name "*vhost*")

    if [ ! -z $FILE_LOCATION ]; then
        return
    fi

    FILE_LOCATION=$(find "$APACHE_CONF_DIR" -name "*virtual*")

    if [ ! -z $FILE_LOCATION ]; then
        return
    fi

    FILE_LOCATION="$APACHE_CONF_DIR/httpd.conf"

    if [ -f "/Applications/XAMPP/xamppfiles/etc/extra/httpd-vhosts.conf" ]; then
        FILE_LOCATION=/Applications/XAMPP/xamppfiles/etc/extra/httpd-vhosts.conf
    fi
}

append_v2.2() {
    read -d '' VIRTUALHOST <<EOF

<VirtualHost *:80>
    DocumentRoot "$PWD/web"
    ServerName $LOCAL_HOST
    ServerAlias www.$LOCAL_HOST
    <Directory "$PWD/web">
        AllowOverride All
        Order Allow,Deny
        Allow from all
    </Directory>
</VirtualHost>
EOF

    sudo -- sh -c "echo '$VIRTUALHOST' >> $FILE_LOCATION"
    information "Installed VirtualHost to $FILE_LOCATION."
}

append_v2.4() {
    read -d '' VIRTUALHOST <<EOF

<VirtualHost *:80>
    DocumentRoot "$PWD/web"
    ServerName $LOCAL_HOST
    ServerAlias www.$LOCAL_HOST
    <Directory "$PWD/web">
        AllowOverride All
        Order Allow,Deny
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>
EOF

    sudo -- sh -c "echo '$VIRTUALHOST' >> $FILE_LOCATION"

    information "Installed VirtualHost to $FILE_LOCATION."
}

append() {
    source ./createfiles/includes.sh

    check_requirements

    source .env

    get_file_location

    vercomp "$APACHE_VERSION" "2.4"

    if [ $? -eq 2 ]; then
        append_v2.2
    else
        append_v2.4
    fi
}
if [ "$1" = "help" ]; then
    echo "Automatically creates an Apache VirtualHost entry for you"
elif [ "$1" = "usage" ]; then
    echo "both"
else
    append
    exit 0
fi