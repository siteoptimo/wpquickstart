#!/bin/bash

check_requirements() {
    if [ ! -f .env ]; then
        errcho "Please set up your environment and create a dotenv file first."
        exit 1
    fi
}

append() {
    source ./createfiles/includes.sh

    check_requirements

    source .env

    sudo -- sh -c "echo -e \"127.0.0.1\\t$LOCAL_HOST www.$LOCAL_HOST\" >> /etc/hosts"
}
if [ "$1" = "help" ]; then
    echo "Auto-updates your /etc/hosts file"
elif [ "$1" = "usage" ]; then
    echo "both"
else
    append
    exit 0
fi