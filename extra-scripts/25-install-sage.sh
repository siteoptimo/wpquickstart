#!/bin/bash

errcho() {
    >&2 echo $@
}

check_requirements() {
    if [ ! -f .env ]; then
        errcho "Please set up your environment and create a dotenv file first."
        exit 1
    fi

    type node > /dev/null 2>&1 || install_node
    update_node
    type gulp > /dev/null 2>&1 || install_gulp
    type bower > /dev/null 2>&1 || install_bower
}

install_node() {
    errcho "Node was not available on your system. Please install it."
    exit 1
}

update_node() {
    sudo npm install -g npm@latest > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        errcho "Failed to update Node."
        exit 1
    fi
}

install_gulp() {
    sudo npm install -g gulp > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        errcho "Failed to install gulp."
        exit 1
    fi
}

install_bower() {
    sudo npm install -g bower > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        errcho "Failed to install bower."
        exit 1
    fi
}

install_sage() {
    cd $THEME_PATH

    # npm install and bower install
    npm install && bower install

    # exit on error
    if [ $? -eq 1 ]; then
        errcho "Failed to install theme."
        exit 1
    fi

    # build theme
    gulp build

    information "Your theme is now installed and ready to go."
    information "To start developing you have to cd into the theme directory and type gulp watch"

    cd $CWD
}

install() {
    # checking requirements
    check_requirements

    # sourcing env file
    source .env

    # performing extra checks before cloning sage
    install_sage
}

if [ "$1" = "help" ]; then
    echo "Installs the sage theme and its dependencies for you"
elif [ "$1" = "usage" ]; then
    echo "both"
else
    install
fi