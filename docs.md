# [SiteOptimo WP Quickstart](https://www.siteoptimo.com)
Welcome to the SiteOptimo WordPress quickstart. This allows you to create a new WordPress deployment environment without a hassle.

## Requirements

* PHP >= 5.4
* node.js - [Download](https://nodejs.org/download/) or use a package manager
* Composer - [Install](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx) (will attempt to install for you)