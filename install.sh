#!/bin/bash

NOCOLOR='\033[0m'
REDCOLOR='\033[37;41m'
GREENCOLOR='\033[37;42m'
WARNING="$REDCOLOR WARNING $NOCOLOR"
PROGRESS="$GREENCOLOR Progress $NOCOLOR"
CURDIR=$(pwd)

if [ ! -f .env ]; then
    echo -e "$WARNING $1 Please set up your environment and create a dotenv file first."
    exit 1
fi

source .env

require_dependency() {
    if ! type "$1" &> /dev/null ; then
        echo -e "$WARNING $1 is a required dependency."
        exit 1
    fi
}

check_mysql() {
    mysql -u$DB_USER -p$DB_PASSWORD -h$DB_HOST $DB_NAME > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        echo -e "$WARNING Your mysql settings are incorrect. Did you create the database?"
        exit
    fi
}
install() {
    echo -n "Welcome to the installer. Please enter your password. "
    sudo -v
    echo -n "\n"

    # check for dependencies
    echo -e "$PROGRESS Checking dependencies"
    require_dependency composer
    require_dependency node

    # update composer
    echo -e "$PROGRESS Updating composer"
    sudo composer self-update > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        echo -e "$WARNING Could not update composer."
        exit
    fi

    # update npm
    echo -e "$PROGRESS Updating node package manager"
    sudo npm install -g npm@latest > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        echo -e "$WARNING Could not update node package manager."
        exit
    fi

    # install bower
    echo -e "$PROGRESS Installing bower"
    sudo npm install -g bower > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        echo -e "$WARNING Could not install bower."
        exit
    fi

    # install gulp
    echo -e "$PROGRESS Installing gulp"
    sudo npm install -g gulp > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        echo -e "$WARNING Could not install bower."
        exit
    fi

    # install project
    echo -e "$PROGRESS Installing project"
    composer install > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        echo -e "$WARNING Could not install project using composer."
        exit
    fi

    chmod +x sync.sh deploy.sh

    ./sync.sh all down

    cd $THEME_PATH

    # Install theme
    echo -e "$PROGRESS Installing theme dependencies"
    npm install > /dev/null 2>&1 && bower install > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        echo -e "$WARNING Could not install theme dependencies."
        exit
    fi

    # Build theme
    echo -e "$PROGRESS Building theme"
    gulp build  > /dev/null 2>&1

    if [ $? -eq 1 ]; then
        echo -e "$WARNING Could not build theme."
        exit
    fi

    # Installing post-merge hook
    echo -e "$PROGRESS Installing post-merge hook"

    cd $CURDIR

    cp -f post-merge .git/hooks/ && chmod +x .git/hooks/post-merge

    chmod +x sync.sh deploy.sh

    ./sync.sh all down

    echo -e "$PROGRESS All done!"

    echo "You can now start developing by running following command:"
    echo "cd $THEME_PATH && gulp watch"
    tput bel
}

install