#!/bin/bash

## Variables

# Colors
NOCOLOR=$'\033[0m'
WHITE_ON_RED=$'\033[37;41m'
WHITE_ON_GREEN=$'\033[37;42m'
BLUE_ON_WHITE=$'\033[37;44m'
BLACK_ON_WHITE=$'\033[30;47m'
WARNING="${WHITE_ON_RED} WARNING ${NOCOLOR}"
SUCCESS="${WHITE_ON_GREEN} SUCCESS ${NOCOLOR}"
INFORMATION="${BLUE_ON_WHITE} INFO ${NOCOLOR}"
PROMPT="${BLACK_ON_WHITE} PROMPT ${NOCOLOR}"

# Vars
BEDROCK_REPO="https://github.com/roots/bedrock.git"
COMPOSER_CMD=$(which composer)
PHP_CMD=$(which php)
BASH_CMD=$(which bash)
CWD=$(pwd)
LOGFILE="$CWD/createfiles/create.log"
MIN_PHP_VERSION="5.4"
TMPERRFILE="$CWD/createfiles/.tmperrorfile"

## Utility functions

# Logging
success() {
    echo -e "$SUCCESS $1"
    echo "[SUCCESS] $1" >> "$LOGFILE"
}

warning() {
    echo -e "$WARNING $1"
    echo "[WARNING] $1" >> "$LOGFILE"
}

information() {
    echo -e "$INFORMATION $1"
    echo "[INFORMATION] $1" >> "$LOGFILE"
}

# Feedback
continue_or_not() {
    read -p "$PROMPT OK? (y/N)" -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[YyJj]$ ]]; then
        return 1
    else
        return 0
    fi
}

# Compares the version.
# http://stackoverflow.com/questions/4023830/bash-how-compare-two-strings-in-version-format/4025065#4025065
vercomp() {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}