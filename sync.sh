#!/bin/bash

source .env

FILE="latest.sql"
FILE_ARCHIVED="${FILE}.tgz"

sync_db_up() {
    echo "Dumping local database in a file..."
    mysqldump -u$DB_USER -p$DB_PASSWORD -h$DB_HOST $DB_NAME > $FILE
    tar -czf $FILE_ARCHIVED $FILE
    echo "Uploading the file using SCP..."
    scp -P $STAGING_SSH_PORT $FILE_ARCHIVED $STAGING_USER@$STAGING_HOST:~/
    echo "Deleting local sql dump."
    rm -f $FILE $FILE_ARCHIVED
    echo "Performing synchronisation on the server..."
    cat ./.env scripts/sync-db.sh | ssh $STAGING_USER@$STAGING_HOST -p$STAGING_SSH_PORT 'bash -s'
    echo "Done."
    tput bel
}

sync_db_down() {
    echo "Poking server..."
    echo "Waiting for the server to dump its database."
    cat ./.env scripts/dump-db.sh | ssh $STAGING_USER@$STAGING_HOST -p$STAGING_SSH_PORT 'bash -s' > /dev/null 2>&1
    echo "Syncing database."
    echo "Downloading latest database version..."
    rm -f latest.*
    scp -P $STAGING_SSH_PORT $STAGING_USER@$STAGING_HOST:~/$FILE_ARCHIVED .
    echo "Emptying local database..."
    TABLES=$(mysql -u$DB_USER -p$DB_PASSWORD -h$DB_HOST $DB_NAME -e 'show tables;' 2>/dev/null | awk '{ print $1}' | grep -v '^Tables')
    for t in $TABLES
    do
        #echo "Deleting $t..."
        mysql -u$DB_USER -p$DB_PASSWORD -h$DB_HOST $DB_NAME -e "drop table $t" 2>/dev/null
    done
    echo "Importing new database into mysql"
    tar -xzf $FILE_ARCHIVED
    mysql -u$DB_USER -p$DB_PASSWORD -h$DB_HOST $DB_NAME < $FILE 2>/dev/null
    wp --path=$WP_PATH search-replace $STAGING_HOST $LOCAL_HOST > /dev/null 2>&1
    echo "Removing files."
    rm -f $FILE $FILE_ARCHIVED
    echo "rm -f $FILE_ARCHIVED" | ssh $STAGING_USER@$STAGING_HOST -p$STAGING_SSH_PORT 'bash -s'
    echo "Done."
    tput bel
}

sync_files_up() {
    echo "Uploading changed files to the server..."
    rsync -chavzP -e "ssh -p$STAGING_SSH_PORT" "${FILES_PATH}/" "$STAGING_USER@$STAGING_HOST:~/${STAGING_FILES_PATH}" > /dev/null 2>&1
    echo "Done."
    tput bel
}

sync_files_down() {
    echo "Downloading changed files from the server..."
    rsync -chavzP --progress -e "ssh -p$STAGING_SSH_PORT" "$STAGING_USER@$STAGING_HOST:~/${STAGING_FILES_PATH}/" "${FILES_PATH}" > /dev/null 2>&1
    echo "Done."
    tput bel
}

print_usage() {
    echo -e "Usage:\n"
    echo "./sync.sh db up: Syncs db up"
    echo "./sync.sh db down: Syncs db down"
    echo "./sync.sh files up: Syncs files up"
    echo "./sync.sh files down: Syncs files down"
    echo "./sync.sh all up: Syncs all up"
    echo "./sync.sh all down: Syncs all down"
    exit 1
}

if [ "$1" = "db" ]; then
    if [ "$2" = "up" ]; then
        sync_db_up
    elif [ "$2" = "down" ]; then
        sync_db_down
    else
        print_usage
    fi
elif [ "$1" = "files" ]; then
    if [ "$2" = "up" ]; then
        sync_files_up
    elif [ "$2" = "down" ]; then
        sync_files_down
    else
        print_usage
    fi
elif [ "$1" = "all" ]; then
    if [ "$2" = "up" ]; then
        sync_db_up
        sync_files_up
    elif [ "$2" = "down" ]; then
        sync_db_down
        sync_files_down
    else
        print_usage
    fi
else
    print_usage
fi