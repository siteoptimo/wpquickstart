#!/bin/bash

source .env

NOCOLOR='\033[0m'
REDCOLOR='\033[37;41m'
WARNING="$REDCOLOR WARNING $NOCOLOR"


print_usage() {
    echo "Usage:\n"
    echo "./deploy.sh staging"
    exit 1
}

has_uncommitted_changes() {
    UNCOMMITTED_CHANGES=$(git status --porcelain)
    if [ -z "$UNCOMMITTED_CHANGES" ]; then
      return 1
    else
      return 0
    fi
}

has_unpushed_commits() {
    UNPUSHED_COMMITS=$(git log --oneline origin/master..master)
    if [ -z "$UNPUSHED_COMMITS" ]; then
        return 1
    else
        return 0
    fi
}

continue_or_not() {
    read -p "Continue? (y/N)" -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[YyJj]$ ]]; then
        exit 1
    fi
}
deploy_staging() {
    if has_uncommitted_changes; then
        echo -ne "$WARNING You still have uncommitted changes. "
        continue_or_not
    fi
    if has_unpushed_commits; then
        echo -ne "$WARNING You still have unpushed commits. "
        continue_or_not
    fi

    echo "Deploying to staging environment..."
    cat ./.env scripts/deploy.sh | ssh $STAGING_USER@$STAGING_HOST -p$STAGING_SSH_PORT 'bash -s' > /dev/null 2>&1
    echo "Done."
    tput bel
}


if [ "$1" = "staging" ]; then
    deploy_staging
else
    print_usage
fi