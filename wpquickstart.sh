#!/bin/bash

### Using this file ###
#
# curl -sS https://bitbucket.org/siteoptimo/wpquickstart/raw/master/wpquickstart.sh > qs.sh && chmod +x qs.sh && ./qs.sh && rm -f qs.sh
#

git init &&
git remote add origin https://bitbucket.org/siteoptimo/wpquickstart.git &&
git fetch --all &&
git checkout -t origin/master &&
rm -rf .git/

chmod +x create.sh && ./create.sh