#!/bin/bash

SITE_PATH=~/public_html

source "${SITE_PATH}/.env"

FILE="latest.sql"
FILE_ARCHIVED="${FILE}.tgz"

if [ ! -f "$FILE_ARCHIVED" ]
then
        echo "File does not exist."
        exit 1
fi

TABLES=$(mysql -u$DB_USER -p$DB_PASSWORD -h$DB_HOST $DB_NAME -e 'show tables;' | awk '{ print $1}' | grep -v '^Tables' )
for t in $TABLES
do
        echo "Deleting $t..."
        mysql -u$DB_USER -p$DB_PASSWORD -h$DB_HOST $DB_NAME -e "drop table $t"
done
tar -xzf $FILE_ARCHIVED

mysql -u$DB_USER -p$DB_PASSWORD -h$DB_HOST $DB_NAME < $FILE
wp --path="${SITE_PATH}/${WP_PATH}" search-replace $LOCAL_HOST $STAGING_HOST > /dev/null 2>&1
rm -f $FILE $FILE_ARCHIVED