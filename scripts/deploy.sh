#!/bin/bash

SITE_PATH=~/public_html
THEME_PATH="${SITE_PATH}/web/app/themes/tandkliniek"

source "${SITE_PATH}/.env"

cd $SITE_PATH
git pull
composer install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --optimize-autoloader
cd $THEME_PATH
npm install
bower install
gulp clean && gulp build --production
cd $SITE_PATH
#wp --path=$WP_PATH total-cache flush db