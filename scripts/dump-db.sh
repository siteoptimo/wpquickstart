#!/bin/bash

SITE_PATH=~/public_html

source "${SITE_PATH}/.env"

FILE="latest.sql"
FILE_ARCHIVED="${FILE}.tgz"

mysqldump -u$DB_USER -p$DB_PASSWORD -h$DB_HOST $DB_NAME > $FILE
tar -czf $FILE_ARCHIVED $FILE
rm -rf $FILE